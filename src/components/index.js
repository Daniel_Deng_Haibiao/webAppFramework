import MTabbar from './packages/MTabbar/index.js'
import MHeader from './packages/MHeader/index.js'

const version = '1.0.0'

const install = function (Vue, config = {}) {
    if (install.installed) return
    Vue.component(MTabbar.name, MTabbar)
    Vue.component(MHeader.name, MHeader)
};

export default {
    version,
    install
}