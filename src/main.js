// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'

//router
import {router} from './router'

//mcomponents
import mcomponents from './components'
Vue.use(mcomponents)

//css
import './static/font/index.css'

//plugin
import plugin from './plugin'
Vue.use(plugin)


Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
