import Vue from 'vue'

export default {
    install(Vue) {
        Vue.mixin({
            methods: {
                goto(value) {
                    this.$router.push(value);
                },
                back() {
                    self.$router.go(-1);
                }
            }
        })
    }
}