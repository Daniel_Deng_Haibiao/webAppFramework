import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

let routes = [
  {
    name: '/',
    path: '/',
    component: (resolve) => {
      require(['../view/index.vue'], resolve)
    }
  }, {
    name: 'shell',
    path: '/shell',
    component: (resolve) => {
      require(['../view/common/shell.vue'], resolve)
    },
    children: [
      {
        name: 'index',
        path: '/index',
        component: (resolve) => {
          require(['../view/index/index'], resolve)
        }
      },
      {
        name: 'contact',
        path: '/contact',
        component: (resolve) => {
          require(['../view/contact/index'], resolve)
        }
      },
      {
        name: 'find',
        path: '/find',
        component: (resolve) => {
          require(['../view/find/index'], resolve)
        }
      },
      {
        name: 'center',
        path: '/center',
        component: (resolve) => {
          require(['../view/center/index'], resolve)
        }
      }
    ]
  }]

export const router = new VueRouter({
  mode: 'history',
  routes
})