
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import utilsModule from './utils/module'

export default new Vuex.Store({
    strict: true,
    modules: {
        utils: utilsModule,
    }
})
