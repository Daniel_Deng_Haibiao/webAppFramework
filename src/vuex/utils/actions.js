import * as types from './mutation-types'
export default {
  storeBackData({ commit }, val) {
    commit(types.STORE_BACK_DATA, val);
  },
  clearBackData({ commit }, val) {
    commit(types.CLEAR_BACK_DATA);
  }
}