import * as types from './mutation-types'
export default {
  [types.STORE_BACK_DATA](state, val) {
    state.backData.target = val.target;
    state.backData.value = val.value;
  },
  [types.CLEAR_BACK_DATA](state) {
    state.backData = null;
  },
}